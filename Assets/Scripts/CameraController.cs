using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform cam;

    private void LateUpdate()
    {
        //This makes camera follow player
        cam.position = transform.position;
        cam.rotation = transform.rotation;
    }
}
