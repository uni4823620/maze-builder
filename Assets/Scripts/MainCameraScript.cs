using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraScript : MonoBehaviour
{
    private Camera cam;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    //Sets Main camera to display on 1st or 2nd Display
    public void TurnOnMainCamera()
    {
        cam.targetDisplay = 1;
    }

    public void TurnOffMainCamera()
    {
        cam.targetDisplay = 2;
    }
}
