using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapGenerator : MonoBehaviour
{ 
    [SerializeField] private List<Texture2D> maps = new List<Texture2D>();
    private Texture2D pickedMap;

    [Header("Tile Prefabs")]
    [SerializeField] private GameObject spawnTile;
    [SerializeField] private GameObject regularTile;
    [SerializeField] private GameObject finishTile;
    [SerializeField] private GameObject wallTile;
    [SerializeField] private GameObject ceilingTile;

    [SerializeField] private GameObject spawner;
    [SerializeField] private GameObject playerManager;

    public void SpawnMaze()
    {
        //This picks a random map from the map pool
        pickedMap = maps[Random.Range(0, maps.Count)];
        
        //Those 2 for loops go through each row of map's Texture and draws a maze according to the instructions
        for (int heightI = 0; heightI < pickedMap.height; heightI++)
        {
            for (int widthI = 0; widthI < pickedMap.width; widthI++)
            {
                Color pixelColor = pickedMap.GetPixel(widthI, heightI);
                Vector3 spawnPosition = spawner.transform.position + new Vector3(widthI * 2, 0, heightI * 2);
                GameObject instantiatedTile = null;

                if (pixelColor == Color.green)
                {
                    instantiatedTile = Instantiate(spawnTile, spawnPosition, Quaternion.identity);
                    playerManager.transform.position = spawnPosition;
                }
                else if (pixelColor == Color.white)
                {
                    instantiatedTile = Instantiate(regularTile, spawnPosition, Quaternion.identity);
                }
                else if (pixelColor == Color.red)
                {
                    instantiatedTile = Instantiate(finishTile, spawnPosition, Quaternion.identity);
                }
                else if (pixelColor == Color.black)
                {
                    instantiatedTile = Instantiate(wallTile, spawnPosition, Quaternion.identity);
                }

                if (instantiatedTile != null)
                {
                    //Here, the tiles are made into children of spawner for easier destroying later
                    instantiatedTile.transform.parent = spawner.transform;
                }
            }
        }

        //This bit spawn a ceiling big enough to cover the whole map
        GameObject ceiling = Instantiate(ceilingTile, new Vector3(spawner.transform.position.x + pickedMap.width,
            2.5f, spawner.transform.position.z + pickedMap.height), Quaternion.Euler(180f, 0f, 0f));
        ceiling.transform.localScale = new Vector3(0.2f * pickedMap.width, 1f, 0.2f * pickedMap.height);
        ceiling.transform.parent = spawner.transform;
    }
}