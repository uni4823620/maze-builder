using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private GameObject _pauseMenu;
    [SerializeField] private GameObject _mazeGenerator;

    public int currentGameState;

    private void Update()
    {
        //Checks for input to turn on Pause Menu
        if(Input.GetKeyDown(KeyCode.Escape) && currentGameState == (int)GameState.playing)
        {
            TurnOnPause();
        }
    }

    private void Start()
    {
        //Makes Main menu be default menu when the game is turned on
        _mainMenu.SetActive(true);
        _pauseMenu.SetActive(false);
        currentGameState = (int)GameState.menu;
    }

    //Turns main menu on
    public void ToMainMenu()
    {
        _mainMenu.SetActive(true);
        _pauseMenu.SetActive(false);
        currentGameState = (int)GameState.menu;
        Time.timeScale = 1f;
        DestroyMaze();
    }

    //Turns off UI for the game
    public void StartGame()
    {
        _mainMenu.SetActive(false);
        _pauseMenu.SetActive(false);
        currentGameState = (int)GameState.playing;
    }

    //Turns on pause menu
    public void TurnOnPause()
    {
        _pauseMenu.SetActive(true);
        currentGameState = (int)GameState.pause;
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
    }

    //Turns off pause menu
    public void TurnOffPause()
    {
        _pauseMenu.SetActive(false);
        currentGameState = (int)GameState.playing;
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
    }

    //Quits the game
    public void QuitGame()
    {
        Application.Quit();
    }

    //Destroys maze
    public void DestroyMaze()
    {
        foreach (Transform child in _mazeGenerator.transform)
        {
            Destroy(child.gameObject);
        }
    }
}

public enum GameState
{
    menu,
    pause,
    playing
}
