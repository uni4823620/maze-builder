using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    private Camera cam;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    //Sets camera to different Displays so that player's camera can be seen from
    public void TurnOnPlayerCamera()
    {
        cam.targetDisplay = 0;
    }
    public void TurnOffPlayerCamera()
    {
        cam.targetDisplay = 1;
    }
}
