using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerManager playerManager;
    [SerializeField] private Transform _playerHead;
    private float xRotation = 0f;
    private float yRotation = 0f;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Camera cam;
    private float _mouseSensitivity;

    private void Awake()
    {
        //This locks the camera, makes the camera display the Player's view
        Cursor.lockState = CursorLockMode.Locked;
        cam.targetDisplay = 0;
    }

    void Update()
    {
        //This makes sure that the player got access to variables from Player Manager
        if(playerManager != null)
        {
            _mouseSensitivity = playerManager.mouseSensitivity;
        }

        //This bit makes player and it's head turn
        float mouseX = Input.GetAxis("Mouse X") * _mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * _mouseSensitivity * Time.deltaTime;
        xRotation += mouseX;
        yRotation -= mouseY;
        yRotation = Mathf.Clamp(yRotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(0f, xRotation, 0f);
        _playerHead.localRotation = Quaternion.Euler(yRotation, 0f, 0f);
        
        //This makes player move according to the side it's facing
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 moveDirection = transform.forward * vertical + transform.right * horizontal;
        Vector3 moveVelocity = moveDirection.normalized * moveSpeed;
        rb.velocity = moveVelocity;
    }

    //This checks for collision with Player Manager game object to get access to it's script
    private void OnTriggerEnter(Collider other)
    {
        playerManager = other.GetComponent<PlayerManager>();
    }

}

