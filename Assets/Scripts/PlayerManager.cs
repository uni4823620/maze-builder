using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private TMP_Text mouseText;
    [SerializeField] private Slider mouseSlider;
    public float mouseSensitivity = 100f;

    void Update()
    {
        //This sets player's mouse sensitivity according to the Slider value
        mouseSensitivity = mouseSlider.value + 50f;
        mouseText.text = "Mouse Sensitivity: " + mouseSlider.value;
    }
}
